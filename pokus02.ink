# theme: dark

VAR panika = 0
VAR spal = false
VAR cokolada_nalezena = false
VAR cokolada_snezena = false
VAR hudba = false
VAR kocicka = false


->intro

===intro===
Ráno tě probudí nepříjemné světlo, které ti svítí do očí. Je 7:26 a dnes tě čeká... počkat, je už 7:26! Dnes nesmíš přijít pozdě. Prostě nesmíš! Vyskočíš z postele, začneš se oblékat a zároveň házet učebnice do tašky.
    *7:28
        Snad máš všechno. Běžíš do předsíně a tam vklouzneš do prvních bot, které vidíš. Pravděpodobně jsou tvoje. Vybíháš ven z domu.
    **7:30
        Když přidáš, ještě bys mohl stihnout...
    ***Prásk!
        Právě jsi narazil do vzduchu. Co se to stalo? Zprvu nic nevidíš, ale když zaostříš zrak asi tak na deset centimetrů před sebe, zjistíš, že tam vzduch podivně poblikává. Uděláš znovu krok dopředu, ale neviditelná zeď tě opět zastaví.
    ****???
        Chvíli jen tak stojíš na místě. Když se rozhlédneš, dojde ti, že vzduch takhle poblikává kolem dokola domu.
        ~change_panic(true)
->intro1
==intro1
*[prohlédnout si bariéru] 
    Prohlížíš si světelé impulzy přebíhající po povrchu neviditelné stěny. Tvou pozornost ale upoutá něco v dálce za bariérou. Intenzivní modré světlo. A postupně přibývají další takové světelné zdroje. A mezi nimi dlouhé hrozivé stíny.
    ~change_panic(true)
    ->intro1
*[dotknout se bariéry]
    Opatrně položíš ruku na bariéru, cítíš jak vibruje.
    **[pokusit se prostrčit prst skrz]
            Pokusíš se prostrčit prst skrz, ale nejde to. Znamená to, že jsi tu uvězněn?
            ~change_panic(true)
            *** [zkusit to větší silou] 
                Zkusíš do bariéry praštit silou. Ta se jen zavlní... a dá ti elektrický šok. Rychle ucukneš.
                ~change_panic(true)
                -> intro1
            *** [nechat bariéru napokoji] -> intro1
* -> 
**[poodstoupit]
    Uděláš pozpátku pár kroků pryč od bariéry. V tu chvíli se světelné impulzy seběhnou do jednoho místa a bariéra vydá nepříjemně vysoký syntetický zvuk. V dálce se stíny začnou pohybovat. ->intro2
==intro2
{|Zdá se, že jeden za stínů se přibližuje k domu.|Není pochyb o tom, že míří směrem k tobě.}
*[vrátit se k bariéře a zkusit si lépe prohlédnout pohybující se stíny]->intro2
*[zamávat na pohybující se stíny]->intro2
*[počkat co se stane] 
    Jak se k tobě stín blíží, začneš v něm rozeznávat jakési zvíře. Něco takového jsi ale nikdy neviděl. Monstrum s obřími pařáty neustále zrychluje a běží směrem k tobě.
    **[utéct dovnitř]    ->predsin
    **[zůstat stát] Pasivně zůstáváš stát na místě a prostě jen doufáš, že bariéra ho zastaví. Nic takového se ale nestane.
        <b>KONEC</b>
        ->END


===predsin===
Vběhneš do předsíně a zabouchneš dveře. Monstrum do nich buší.
-> predsin1
==predsin1
+{panika<=5}[zírat na dveře]
    {panika==5: Rozklepaně z|Z}íráš na dveře.
    ~change_panic(true)
    ->predsin1
*{panika<=5}[zamknout dveře]
    Pokusíš se zasunout klíč do klíčové dírky, ale příliš se ti klepou ruce.
    ~change_panic(true)
    ->predsin1
*{panika<=5}[nakouknout do klíčové dirky]
    Nic není vidět, už odstupuješ od dvěří, když se na druhé straně klíčové dirky objeví obrovské žluté oko. Odskočíš od dveří.
    ~change_panic(true)
    ->predsin1
*{panika<=5}[porozhlédnout se po místnosti]
    Zvuk pařátů škrábajících o dveře je nesnesitelný, panikaříš, nemůžeš se soustředit.
    ~change_panic(true)
    ->predsin1
*{panika>5}[utéct do koupelny]->zachod

===zachod===
Schováš se na záchodě a okamžitě za sebou zamkneš dveře. Nevěříš ale, že to něčemu pomůže.
->zachod1
==zachod1
*[poslouchat] 
    Slyšíš, jak monstrum prorazilo vstupní dveře. Jeho drápy teď skřípou o dlaždičky v koupelně. Najednou cítíš, že se ti něco lepkavého otřelo o levý kotník. 
        **[podívat se dolu]
            Celá podlaha je pokryta vrstvou modrého slizu. Navíc se zdá, že sliz se pohybuje. Začíná se ti nabalovat na nohy. V tom uslyšíš hlasitou ránu. Monstrum už se dostalo ke dveřím a snaží se je vyrazit.
            ***(hledal)[hledat únikovou cestu] 
                Rozhlédneš se rychle po místnosti, ale až na malé okénku, kudy bys neprotáhl ani hlavu, tu nic není. Sliz se ti mezitím úspěšně obalil okolo kotníků. Vytáhneš pracně nohy z bot, vyskočíš
            ***[vyskočit na záchodovou mísu]
                 Vyskočíš
            ---<> rychle nahoru na záchodovou mísu a pak zíráš na slizká chapadla, která se na tebe snaží dosáhnout. {hledal: Tvoje boty už úplně zmizely pod hladinou.}
                ~change_panic(true)
            ***[čekat]
                Krčíš se na záchodové míse, monstrum řve, dveře se otřásají a chapadla už ti opět dosahují ke kotníkům. V tom se odněkud zboku vynoří obrovská verze slizovitého chapdla a srazí tě na podlahu.
                ~change_panic(true)
                Omdléváš.
                ->zachod2
==zachod2
*...
**...
***...
Po nějaké době se probouzíš na podlaze. Chapadla jsou pryč a monstrum už také není slyšet.
****[otevřít dveře] Opatrně otevřeš dveře. Nikde nikdo.
~panika = 4
<b>úroveň paniky klesá na {panika}</b> # CLASS: ok
->predsin2

===predsin2===
Vrátíš se do předsíně.
Vstupní dveře jsou rozlámané, koberec rozdrásaný od pařátů nestvůry. Na zemi si všimneš bahnitých stop vedoucích do kuchyně.
*[nahlédnout do kuchyně]
    Opatrně nahlédneš do kuchyně. Monstrum je asi metr od tebe. Naštěstí k tobě stojí zády a ty zvládneš rychle vycouvat zpět do předsíně.
    ~change_panic(true)
    ->predsin2
*[jít do svého pokoje]
    Uděláš dva kroky směrem k pokoji a pod nohama ti zavržou parkety. Ztuhneš na místě, ale jen na krátký okamžik. Reakce monstra je okamžitá – vybíhá z kuchyně a už je ti v patách.
    ~change_panic(true)
    ->pokoj

===pokoj===
Doběhneš do pokoje a zabouchneš za sebou dveře a zatarasíš je nočním stolkem. Pak se rozhlédneš po místnosti.
->pokoj_popis
==pokoj_popis
Je tu totální chaos. Věci, které ještě ráno ležely poházené po stole a podlaze teď levitují těsně pod stropem.
->pokoj2
==pokoj2
{panika>=8: ->omdleni}
+[zkontrolovat dveře]
    Tyhle dveře monstrum moc dlouho neudrží.
    ~change_panic(true)
    ->pokoj2
+[použít počítač]
    ->pocitac
+[pokusit se pochytat levitující předměty]
    Natáhneš ruku směrem ke knížce, která poletuje nejblíž. Ta okamžitě poposkočí do výšky a s hlasitou ranou narazí do stropu. {spal: Všimneš si, že jedním z levitujícíh předmětů je tvůj telefon. Proč sis ho ráno nevzal s sebou?}
    **[chytit knížku]
        Vylezeš na postel a snažíš se dosáhnout na knížku, která je nalepená na stropě. V tom se předměty zastaví na místě. Chvíli se nic neděje, a pak se na tebe všechny najednou vrhnou.
        ~change_panic(true)
        ->pokoj2
    ++{not kocicka}[chytit nějaké poletující papíry]
        Chytl jsi fotku kočičky. Svými roztomiloučkými očíčky by rozněžnila kohokoliv… a cokoliv. Myšlenka, že bys použil tuto fotku na zpřátelení toho obřího monstra, tě donutí se pousmát. Pak ti fotka vyklouzne z prstů a připojí se zpět k chumlu poletujících papírů.
        ~change_panic(false)
        ~kocicka = true
        ->pokoj2
    ++{spal}[chytit telefon]
        Opatrně natáhneš ruku ke svému telefonu.
        {panika>=2: 
            Ale je příliš vysoko.
            ->pokoj2
        }
        {panika==1: Ten se dvakrát rychle otočí na místě a pak ti pomalu sklouzne do nastavné dlaně.}
        ->ending
    ++[nechat předměty levitovat]->pokoj2
+[prohledat šuplíky]
    Tvoje věci poletují všude kolem. Snad alespoň ty zavřené v šuplících zůstaly na svém místě.
    ->supliky1
+{not spal}[jít si lehnout]
    {panika==2: 
        Lehneš si na postel a zavřeš oči. Monstrum stále tluče do dveří, ale zdá se, že mu ubývají síly. Za chvíli je z ohlušujících ran jen tiché ťukání. Místo toho se tu objeví úplně nový zvuk. Zní odněkud od stropu. Zní jako... tvůj telefon.
        ~change_panic(false)
        ~spal = true
    -else:
        Lehneš si na postel, zavřeš oči a snažíš se předstírat, že žádné problémy neexistují. Tvé panika je ale příliš vysoká, nedokážeš se soustředit na nic jiného, než na zvuk nestvůry narážející do dveří.
    }
    ->pokoj2
==supliky1
    *(prvni)[otevřít první šuplík]
        Vyvalí se na tebe záplava propisek. Okamžitě se přidají k chumlu ostatních předmětů poletujících po pokoji.
        ->supliky1
    *(druhy){prvni}[otevřít druhý šuplík]
        Šuplík je celý ulepený. K jeho dnu je přilepená mírně rozteklá čokoláda.
        **[sníst kousek čokolády]
            ~change_panic(false)
            ~cokolada_nalezena = true
            ~cokolada_snezena = true
        ->supliky1
    *{druhy}[otevřít třetí šuplík]
        Šuplík je plný nožů. Cože? Kde se ve tvém pokoji vzal šuplík plný nožů? Nemáš nad tím ale moc šanci přemýšlet, protože nože okamžitě začínají vyletovat ven a ty musíš rychle uhýbat.
        ~change_panic(true)
        ->pokoj2
    +{cokolada_nalezena && not cokolada_snezena}[sníst kousek čokolády]
        ~change_panic(false)
        ~cokolada_snezena = true
        {panika>=10: ->omdleni}
        ->supliky1
    +[odejít]->pokoj2
    

==pocitac
{panika>=8: ->omdleni}
Zkoušíš vyhledat nějaké informace:
*["jak se zbavit levitujících předmětů"] 
    <i>"Levitace je všeobecně známý jev způsobující létání různých předmětů, lidí, prasopsů i jiných objektů bez zjevných příčin..."</i>
    ~change_panic(true)
    ->pocitac
*["jak správně zabarikádovat dveře"] 
    <i>7 EFEKTIVNÍCH ZPŮSOBŮ JAK ZAMKNOUT DVEŘE BEZ ZÁMKU</i>
    <i>"Barikáda je jedním z nejúčinnějších způsobů, jak zajistit dveře, když potřebujete další bezpečnost. Metoda je však obvykle účinná pro dveře, které se otevírají dovnitř. Abyste zabarikádovali dveře, musíte být také uvnitř místnosti..."<i>
    ~change_panic(true)
    ->pocitac
*(druhy)["druhy monster"]
++["minotauři"] <i>"Blabalbalbalbalablablablablba"</i> ->druhy
++[{&"cthulhu"|"cathulhu"|"chthulhu"|"ctulhu"}] Klepou se ti prsty, tohle slovo nejsi schopen napsat.->druhy
++[nechat toho]
    ~change_panic(true)
    ->pocitac
+{not hudba}[pustit hudbu]
    Pustíš hudbu. Zdá se, že poletující předměty na ni reagují. Zpomalily svůj pohyb a klesly o kousek níž.
    ~hudba = true
    ~change_panic(false)
    ->pocitac
+[odejít]->pokoj2


===omdleni===
<b<Úroveň paniky dosáhla 8</b> # CLASS: panika
Omdléváš.
+...
++...
Po nějaké chvíli se probouzíš na podlaze svého pokoje.
~panika = 5
<b>úroveň paniky klesá na {panika}</b> # CLASS: ok
~cokolada_snezena = false
~spal = false
~hudba = false
~kocicka = false
->pokoj_popis

===ending===
Telefon stále vyzvání. Přijmeš hovor.
+"Haló? Babi?"
.......

++"Ne, jsem doma, já totiž nemůžu..."
.......

+++"Ne, nebojím se zkoušky, já tady mám..."
(v tu chvíli se všechny věci viditelně hnuly směrem k podlaze)
.......

++++"Cože? Jaký svátek?
.......

+++++"Jo aha, tak moc děkuju"
(zdá se mi to, nebo je všechno zas o kousek níž?)

++++++[přepnout na hlasitý odposlech]
Místnost náhle zaplní babiččin hlas:
<i>"A nezapomeň si vzít deštník, dneska má pršet. A odpoledne se u mě stav pro bábovku a... jsi tam ještě?"</i>

+++++++"Jsem"[] hlesneš. " Díky, babi, ani nevíš jak moc jsi mi pomohla. Já ti pak ještě zavolám".

Sedíš na posteli a nevěřícně zíráš, všechny předměty se během hovoru snesly ze vzduchu a zaujaly svá původní místa. 

<b>KONEC</b>

->END

=== function change_panic(increase) ===
    {increase:
        ~panika++
    -else:
        ~panika--
    }
    {panika==8:
        <b>úroveň paniky dosáhla {panika}</b> # CLASS: panika
    -else:
        {panika>=6:
            <b>úroveň paniky {increase:stoupla|klesla} na {panika}</b> # CLASS: panika
        -else:
            {panika>=3:
                <b>úroveň paniky {increase:stoupla|klesla} na {panika}</b> # CLASS: ok
            -else:
                <b>úroveň paniky {increase:stoupla|klesla} na {panika}</b> # CLASS: klid
            }
        }
    }
    
/*    
1) technická oprava cyklení omdlévání (při třetím omdlení v řadě se to zasekne)
2) vyjádřit úroveň paniky nějak lépe (bez čísla? barevné číslo podle stopula/klesla?), a je potřeba 10 úrovní, nestačilo by méně?
3) smrt ze zamykání dveří je zlá/příliš zákeřná
3.1) celkově dořešit umírání: má tam být? proč? jak aby návrat nenudil? co znamená z hlediska děje?
4) naučit hráče, že se volby odemykají či zamykají dle úrovně paniky (důležité!!!!)
4.1) proč se otevře útěk do koupelny až když je panika vysoká? 
5) je tam mockrát opakované klišé zamčených dveří
6) je to moc těžké (ve fázi pokoj)
7) dopsat závěrečný telefonát

1) HOTOVO
2) zkusila bych místo úroveň je X psát panika stoupla/klesla na X a psát to okamžitě po změně (a ne když zůstala stejná)(HOTOVO), možná pak odstíny červené (?), počet úrovní dost závosí na bodu 6
3) tu chci vyhodit (HOTOVO), ale stále tam ke zvážení zbývá ta první možná smrt venku...
3.1) ???
4) možná by tam měly být vidět celou dobu, ale být třeba přeškrtnuté (a při kliknutí řeknou něco na téma "na tohle jsi příliš rozklepaný..." a až bude panika na dostatečně nízké/vysoké úrovni, tak se to odemkne a přestane ten text té volby být přešktnutý)
    Ale zase co je možnost, která s panikou mizí? Jen lehnutí si na postel vlastně...
    ???
    Vlastně by také bylo fajn, kdyby ten text vypadal trochu jinak podle toho, jaká je aktuální panika...
4.1) kdyby to bylo viz předchozí bod, tak opravdu nevím, co sem napsat jako důvod
5) první možnost co mě napadá je prostě vynechat tu scénu s posouváním skříně/nočního stolku a prostě jen napsat "Vběhl jsi do pokoje a dveře zatarasil nočním stolek. Pak ses rozhlédl, všechno levituje." (HOTOVO)
6) ??? Pro usnadnění by mohlo jít nechat zmizet možnosti, jak si zvýšit paniku, které už jsi někdy zkoušel. (ale tam pak nastan situace, že ti zbyde jedna negativní akce, ty ji omylem uděláš a o jedna ti to nevyjde, takže to musíš opakovat znova, tj. v tu chvíli se musíš dobrovolně schválně vystresovat nějakou jednou permanentně dostupnou negativní volbou ala zírání na dveře).
    Také není jasně vidět, na kolikátce má panika být, aby si šlo jít lehnout (?).
7) HOTOVO


*/





